;; This will be overwritten if the user customises the agda2-program-args
;; variable in his/her own session. But that is probably the intended behaviour
;; anyway.

(setq agda2-program-args '("-i." "-i/usr/share/agda-stdlib"))
